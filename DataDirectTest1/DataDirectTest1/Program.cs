﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Odbc;

namespace DataDirectTest1
{
    class Program
    {
        static void Main(string[] args)
        {

            /*Driver connection parameters:
             * DRIVER={DataDirect 7.1 OpenEdge Wire Protocol} -- The exact driver name can be found in the Control Panel ODBC connections interface.  Be sure to update the version number if necessary.
             * HOST=localhost; -- supply the address to the OpenEdge database
             * PORT=8600; -- Supply the database port.  Can be found in the OpenEdge Explorer.
             * DB=Sports2000; -- the name of the database.
             * UID=***username***; -- if database was created on windows with prodb command, the username is the same as that of the person who ran the prodb command.
             * PWD=; -- if database was created on Windows with prodb command, the password is blank.
             * DIL=READ UNCOMMITTED;
             * 
             * Note, by creating a DSN in the ODBC driver manager in windows, the connection can be tested.
            */

  
            var myConnectionString = ("DRIVER={DataDirect 7.1 OpenEdge Wire Protocol};HOST=localhost;PORT=8600;DB=Sports2000;UID=***username***;PWD=;DIL=READ UNCOMMITTED;");


            //var MySelectQuery = "SELECT Custnum, Name, City FROM PUB.Customer WHERE CustNum < 3";
            var MySelectQuery = "SELECT PUB.Customer.Custnum, Name, City, PUB.Order.OrderNum, PUB.Order.Terms FROM PUB.Customer INNER JOIN PUB.Order On Pub.Customer.Custnum = Pub.Order.CustNum WHERE Pub.Customer.CustNum < 3";

            using (var myConnection = new OdbcConnection(myConnectionString))
            using (var MyCommand = new OdbcCommand(MySelectQuery, myConnection))
            {

                myConnection.Open();

                using (var myReader = MyCommand.ExecuteReader())
                {

                    while (myReader.Read()) {
                        Console.WriteLine(myReader.GetString(0) + "  " + myReader.GetString(1) + "  " + myReader.GetString(2) + "  " + myReader.GetString(3) + "  " + myReader.GetString(4));
                    } 
                }
            }

            Console.ReadKey();

        }
    }
}
